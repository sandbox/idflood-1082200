The haml_base5 theme is a basic theme using the peroxide theme engine.

To use this theme you must download and install the peroxide theme
engine which can be found at https://github.com/codeincarnate/peroxide
