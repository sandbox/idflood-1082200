<?php
function haml_base5_form_system_theme_settings_alter(&$form, $form_state) {
  $form['haml_base5'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('haml_base5 settings'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    "#weight"        => -15,
  );
  $form['haml_base5']['haml_base5_modernizr'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Modernizr'),
    '#default_value' => theme_get_setting('haml_base5_modernizr'),
    '#description'   => t('Enable modernizr javascript. This script enable html5 elements styling in internet explorer among many other cool stuff.<br/><a href="http://www.modernizr.com/">www.modernizr.com</a>'),
  );
  $form['haml_base5']['haml_base5_dd_belatedpng'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('DD_belatedPNG'),
    '#default_value' => theme_get_setting('haml_base5_dd_belatedpng'),
    '#description'   => t('Enable DD_belatedPNG javascript for internet explorer 6. It fixes the transparency of png images.<br/><a href="http://www.dillerdesign.com/experiment/DD_belatedPNG/">www.dillerdesign.com/experiment/DD_belatedPNG</a>'),
  );
}
